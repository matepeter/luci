module("luci.controller.zorp", package.seeall)

function index()
	entry({"admin", "network", "zorp"},
		alias("admin", "network", "zorp", "policies"),
		_("Zorp"), 80)

	entry({"admin", "network", "zorp", "instances"},
		cbi("zorp/instances"),
		_("Instances"), 20).leaf = true

	entry({"admin", "network", "zorp", "policies"},
		cbi("zorp/policies"),
		_("Policies"), 10).leaf = true
end
