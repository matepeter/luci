--[[
LuCI - Lua Configuration Interface

Copyright 2011 Jo-Philipp Wich <xm@subsignal.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id$
]]--

local fs = require "nixio.fs"

local f = SimpleForm("zorp",
	translate("Zorp - Instances"),
	translate("Edit zorp instances.conf, sends zorpctl restart on submit"))

local o = f:field(Value, "_policies")

o.template = "cbi/tvalue"
o.rows = 20

function o.cfgvalue(self, section)
	return fs.readfile("/etc/zorp/instances.conf")
end

function o.write(self, section, value)
	value = value:gsub("\r\n?", "\n")
	fs.writefile("/etc/zorp/instances.conf", value)
	luci.sys.exec("zorpctl restart")
end

return f
